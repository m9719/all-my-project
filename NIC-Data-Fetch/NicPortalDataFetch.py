import os
import re
import time
import webbrowser
from datetime import datetime, timedelta
import multiprocessing as mp
from selenium.webdriver.support import expected_conditions as EC
import pandas as pd
from selenium.webdriver.support.wait import WebDriverWait

import data_google_sheet_ops2 as gs
from gspread_dataframe import set_with_dataframe
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.common.exceptions import StaleElementReferenceException
from bs4 import BeautifulSoup
import json

def save_each_link(link, name, searchkey, sheetname, static_html_file_path, index):
    filepath = static_html_file_path  # "D:\\NIC-Data-Fetch\\NIC Static Site\\"#os.path.abspath("__file__")
    make_directory(os.path.join(filepath, current_date.split("-")[0]))
    fpath = []

    try:
        # driver.get(link)
        # Save the HTML source code to a file
        folder_path = os.path.join(filepath, current_date.split("-")[0])
        filename = f"{name}_{searchkey}_{index}.html"
        file_path = os.path.join(folder_path, filename)
        # time.sleep(4)

        with open(file_path, "w", encoding="utf-8") as file:
            file.write(driver.page_source)
        fpath.append(filepath)
    except StaleElementReferenceException:
        # Handle stale session by restarting
        handle_stale_session()

def findeachlink(name, searchkey, sheetname, exclude_value, url):
    all_links = []  # List to store links across multiple pages
    while True:
        updated_content = driver.page_source
        soup = BeautifulSoup(updated_content, 'html.parser')
        tbody = soup.find('tbody')
        even_odd = tbody.find_all('tr', class_=["odd", "even"])
        lnk = []
        for row in even_odd:
            tds = row.find_all('td')
            for td in tds:
                try:
                    links = td.find_all('a', href=True)
                    for link in links:
                        href = link.get('href')
                        link_text = link.get_text()
                        if exclude_value != [''] or not None:
                            # exclude_check = any(value.lower() in link_text.lower() for value in exclude_value)
                            exclude_pattern = re.compile(r'\b(?:' + '|'.join(map(re.escape, exclude_value)) + r')\b',
                                                         flags=re.IGNORECASE)
                            exclude_check = exclude_pattern.search(link_text)
                            if not exclude_check and href:
                                cleaned_url = re.sub(r'\.gov\.in.*$', '.gov.in', url)
                                print("Link Not Excluded", cleaned_url + href)

                                print("Link Not Excluded", link_text)
                                lnk.append(cleaned_url + href)
                        else:
                            if href:
                                cleaned_url = re.sub(r'\.gov\.in.*$', '.gov.in', url)
                                print("Link Not Excluded",cleaned_url+href)
                                print("Link Not Excluded", link_text)
                                lnk.append(cleaned_url+href)

                except StaleElementReferenceException:
                    pass
                    # Handle stale session by restarting
                    handle_stale_session()
        # Append links from the current page to the overall list
        all_links.extend(lnk)

        next_page_link = get_next_page_link()  # Implement this function to get the next page link

        if next_page_link:
            cleaned_url = re.sub(r'\.gov\.in.*$', '.gov.in', url)
            # print("Current URL: ",url)
            # print("Cleaned URL: ",cleaned_url)
            print("Next Page Link: ",cleaned_url +next_page_link)

            driver.get(cleaned_url + next_page_link)
            WebDriverWait(driver, 15).until(
                EC.presence_of_element_located((By.TAG_NAME, 'body'))
            )
            time.sleep(1)
        else:
            # No next page, break out of the loop
            break

    return all_links
def get_next_page_link():
    try:
        updated_content = driver.page_source
        soup = BeautifulSoup(updated_content, 'html.parser')

        next_page_link = soup.find('a', id='linkFwd')
        if next_page_link:
            next_page_url = next_page_link.get('href')
            print("Next Page Link:", next_page_url)
            return next_page_url
        else:
            print("No Next Page Link Found")

    except:
        pass


def handle_stale_session():
    restart_link = driver.find_element(By.ID, "restart")
    restart_link.click()


def make_directory(path):
    """This function is used to create folder if not exists"""
    if not os.path.exists(path):
        os.makedirs(path)


def time_taken(start):
    end_time_ = time.strftime("%H-%M-%S")
    total_time = (datetime.strptime(end_time_, '%H-%M-%S') - datetime.strptime(start, '%H-%M-%S')).seconds
    total_time = time.strftime("%H-%M-%S", time.gmtime(total_time))
    return end_time_, total_time


def opensuburl(lnk, name, searchkey, sheetname, excluded_values):
    # entries_without_spaces = [entry.strip() for entry in search_field_values_list]

    entries = ["Tender ID", "Work Description", "Organisation Chain", "Bid Submission End Date", "Tender Value in ₹",
               "EMD Amount in ₹", "Tender Fee in ₹"]
    # print("Entries", entries)
    data_list = []

    for index, link in enumerate(lnk, start=1):
        driver.get(link)
        # time.sleep(3)
        WebDriverWait(driver, 15).until(
            EC.presence_of_element_located((By.TAG_NAME, 'body'))
        )
        time.sleep(1)
        # try:
        #     if save_html_page.lower() == "yes":
        #          save_each_link(link, name, searchkey, sheetname, static_html_file_path, index)
        # except:
        #     pass
        updated_content = driver.page_source
        soup = BeautifulSoup(updated_content, 'html.parser')

        entry_dict = {}

        for index, entry in enumerate(entries):
            try:
                # Find the first "td_caption" element containing the specified entry text
                tender_element = soup.find('td', {'class': 'td_caption'}, string=str(entry))
                td_field_element = tender_element.find_next('td', {'class': 'td_field'})
                # Access the text of the "td_field" element
                tender_value = td_field_element.get_text(strip=True)
                if entry == "Work Description" and excluded_values != ['']:
                    # exclude_check = any(value.lower() in tender_value.lower() for value in excluded_values)
                    exclude_pattern = re.compile(r'\b(?:' + '|'.join(map(re.escape, excluded_values)) + r')\b',
                                                 flags=re.IGNORECASE)
                    exclude_check = exclude_pattern.search(tender_value)
                    if exclude_check:
                        break
                entry_dict["Current_Date"] = current_date
                # print("Current Date: ", current_date)
                if entry == "Bid Submission End Date":
                    bid_submission_end_date = tender_value.split(" ")[0].__str__()
                    bid_submission_end_time = ' '.join(tender_value.split(" ")[1:])
                    entry_dict["Bid Submission End Date"] = bid_submission_end_date
                    entry_dict["Bid Submission End Time"] = bid_submission_end_time
                    entry_dict["Name of Site"] = name
                    entry_dict["Search Key"] = searchkey
                    print("Bid Submission End Date: ", bid_submission_end_date)
                    print("Bid Submission End Time: ", bid_submission_end_time)
                    # print("Name of Site: ",name)
                    # print("Search Key: ",searchkey)
                else:

                    entry_dict[entry] = tender_value
                    entry_dict["Name of Site"] = name
                    entry_dict["Search Key"] = searchkey
                    entry_dict["Link"] = link
                    print("Name of Site: ", name)
                    print("Search Key: ", searchkey)
                    time.sleep(1)
                    print(entry, ": ", tender_value)
            except:
                tender_elements = soup.find_all(
                    lambda tag: tag.name == 'td' and 'td_caption' in tag.get('class',
                                                                             []) and f'{str(entry)}' in tag.text)


                for tender_element in tender_elements:
                    # Find the corresponding "td_field" element
                    td_field_element = tender_element.find_next('td', {'class': 'td_field'})
                    tender_value = td_field_element.get_text(strip=True)
                    if entry == "Work Description" and excluded_values != ['']:
                        # exclude_check = any(value.lower() in tender_value.lower() for value in excluded_values)
                        exclude_pattern = re.compile(r'\b(?:' + '|'.join(map(re.escape, excluded_values)) + r')\b',
                                                     flags=re.IGNORECASE)
                        exclude_check = exclude_pattern.search(tender_value)
                        if exclude_check:
                            break
                    entry_dict["Current_Date"] = current_date

                    if entry == "Bid Submission End Date":
                        bid_submission_end_date = tender_value.split(" ")[0].__str__()
                        bid_submission_end_time = ' '.join(tender_value.split(" ")[1:])
                        entry_dict["Bid Submission End Date"] = bid_submission_end_date
                        entry_dict["Bid Submission End Time"] = bid_submission_end_time
                        entry_dict["Name of Site"] = name
                        entry_dict["Search Key"] = searchkey
                        print("Bid Submission End Date: ", bid_submission_end_date)
                        print("Bid Submission End Time: ", bid_submission_end_time)

                    else:
                        entry_dict[entry] = tender_value
                        entry_dict["Name of Site"] = name
                        entry_dict["Search Key"] = searchkey
                        entry_dict["Link"] = link
                        print(entry, " :", tender_value)
                        print("Name of Site: ", name)
                        print("Search Key: ", searchkey)
                        time.sleep(1)
        if len(entry_dict) > 5:
            data_list.append(entry_dict)
    df = pd.DataFrame(data_list)
    # df.to_csv(f"Tender-{name}-{searchkey}.csv", index=False)
    sheet_name = sheetname
    Worksheet_name =config["NICPortal_Output_Tab_Name"]# "NIC Tender Output Details"
    gs_.add_data_(df, sheet_name=sheet_name, worksheet_name=str(Worksheet_name), header=False, spacing=0,
                  table_range=None)



if __name__ == '__main__':
    # Open and read the JSON file
    with open('config.json', 'r') as file:
        config= json.load(file)
    start_time_ = time.strftime("%H-%M-%S")
    current_date = datetime.now().strftime("%Y%m%d-%H:%M")
    previous_date = datetime.now() - timedelta(days=1)
    previous_date = previous_date.strftime("%Y%m%d")
    mp.freeze_support()
    gs_ = gs.GoogleSheetOps(service_account_key=r"creds.json")
    service_account_key = r"creds.json"
    existing_sheet_url = config["NICPortal_Sheet_Link"]#"https://docs.google.com/spreadsheets/d/1IzpGqVIKDqeb-QwqQL5E5uX6u32-stge46vpdCNz4lk/edit#gid=494796437"
    print("Sheet Link", existing_sheet_url)
    # other_path_sheet = gs_.get_worksheet_as_df(sheet_name=existing_sheet_url, worksheet_name="Other Paths")
    chrome_driver_path = r"./Driver/chromedriver.exe"#other_path_sheet["Chrome Driver Path"][0]
    # static_html_file_path = other_path_sheet["Static HTML Site Path"][0]
    webdriver_path = chrome_driver_path  # "D:\\C-Driver\\chromedriver.exe"
    chrome_options = webdriver.ChromeOptions()
    # Set the path to the ChromeDriver executable
    chrome_options.add_argument(f"webdriver.chrome.driver={webdriver_path}")

    # Initialize the webdriver
    nic_input_tab=config["NICPortal_Input_Tab_Name"]
    driver = webdriver.Chrome(options=chrome_options)
    sheet = gs_.get_worksheet_as_df(sheet_name=existing_sheet_url, worksheet_name=nic_input_tab)
    # save_html_page = sheet['Save Static HTML Page'][0]
    for index, row in sheet.iterrows():
        site_url_value = row['Site Url']
        search_key_value = row['Search Keys']
        search_key_values_list = search_key_value.split(',')
        state_name_value = row['State Name']
        exclude_name_value = row['Exclude']
        excluded_values = exclude_name_value.split('|')
        eachrow = row
        # print(row)
        sh = gs_.sa.open_by_url(existing_sheet_url)
        sheetname = existing_sheet_url
        site_url = site_url_value  # "https://wbtenders.gov.in"
        searchkeys = search_key_values_list  # ["wire", "conductor"]
        for searchkey in searchkeys:
            url = site_url
            driver.delete_all_cookies()
            driver.get(url)
            name = state_name_value  # site_url.removeprefix("https://").removesuffix(".gov.in")
            # driver.implicitly_wait(5)
            time.sleep(2)
            WebDriverWait(driver, 15).until(
                EC.presence_of_element_located((By.TAG_NAME, 'body'))
            )
            search_bar = WebDriverWait(driver, 15).until(
                EC.presence_of_element_located((By.ID, "SearchDescription"))
            )
            search_bar.send_keys(searchkey)
            search_bar.send_keys(Keys.RETURN)
            time.sleep(4)
            lnk = findeachlink(name, searchkey, sheetname, excluded_values, url)
            print("State Name: ",state_name_value,"Search Key: ",search_key_value,"Total links: ",len(lnk))
            opensuburl(lnk, name, searchkey, sheetname, excluded_values)

    driver.quit()
    end_time, total_time_taken_ = time_taken(start=start_time_)
    print("Total_time_taken for nic data fetching", total_time_taken_)

