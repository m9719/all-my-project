from datetime import datetime

import gspread
from oauth2client.service_account import ServiceAccountCredentials

# Authenticate with Google Sheets using service account credentials
scope = ["https://spreadsheets.google.com/feeds", "https://www.googleapis.com/auth/drive"]
creds = ServiceAccountCredentials.from_json_keyfile_name("creds.json", scope)
client = gspread.authorize(creds)

# Open the Google Sheet by title
spreadsheet = client.open_by_url("https://docs.google.com/spreadsheets/d/1IzpGqVIKDqeb-QwqQL5E5uX6u32-stge46vpdCNz4lk/edit#gid=1872555562")

# Select the worksheet by title
worksheet = spreadsheet.worksheet("Sheet19")
current_date = datetime.now().strftime("%d-%b-%Y")

# worksheet.append_rows([[current_date]])
worksheet.update_cell(1, 2, current_date)