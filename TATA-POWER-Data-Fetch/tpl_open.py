# from selenium import webdriver
#
# # Set up the Chrome WebDriver (make sure ChromeDriver is installed and in your PATH)
# driver = webdriver.Chrome()
#
# # Navigate to the website
# tp_nodl = "https://tender.tpnodl.com/tender.aspx"
# driver.get(tp_nodl)
#
# # Find all table rows using XPath
# table_rows = driver.find_elements("xpath", "//html/body/div[2]/table/tbody/tr")
#
# # Iterate through each table row and print its text
# for index, row in enumerate(table_rows, start=1):
#     row_text = row.text
#     print(f"Table Row {index}: {row_text}")
#
# # Close the browser
# driver.quit()
import pandas as pd
from selenium import webdriver
from bs4 import BeautifulSoup
tp_souther="https://admin.tpsouthernodisha.com/tender.aspx"
webdriver_path = "D:\\C-Driver\\chromedriver.exe"  # Adjust the path to chromedriver.exe
tp_nodl = "https://tender.tpnodl.com/tender.aspx"

chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument(f"webdriver.chrome.driver={webdriver_path}")

driver = webdriver.Chrome(options=chrome_options)
driver.get(tp_nodl)

# Wait for some time to ensure the page is loaded; you might need to adjust this based on your requirements
driver.implicitly_wait(10)

updated_content = driver.page_source
soup = BeautifulSoup(updated_content, 'html.parser')
body = soup.find('tbody')
tr = body.find_all('tr')
lnk=[]
for row in tr:
    tds = row.find_all('td')
    td_texts = [td.text.strip() for td in tds]
    # print(td_texts)
    lnk.append( td_texts)
df=pd.DataFrame(lnk)
# for i in df[0]:
#     print(i)
df=df.iloc[:, :3]
column_names = ['DESCRIPTION', 'PACKAGE REFERENCE NO',"BID SUBMISSION / TENDER FEE DUE DATE"]
df.columns = column_names
df.to_csv("tpl.csv",index=False)
driver.quit()
