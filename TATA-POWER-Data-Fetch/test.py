from bs4 import BeautifulSoup

html = '''
<tr>
    <td width="342" height="86" style="border-left-style:solid; border-left-width:1px; border-right-style:none; border-right-width:medium; border-top-style:solid; border-top-width:1px; border-bottom-style:solid; border-bottom-width:1px">
        <p style="margin-top: 0; margin-bottom: 0; text-align:left">
            <font size="2">Tender No. :TPCODL / CCG / 23-24 / 1000000540 &nbsp;&nbsp;</font><font size="2" color="#CC0000">Dt.16/01/2024</font>
        </p>
        <p style="margin-top: 0; margin-bottom: 0; text-align:left">&nbsp;</p>
        <p style="margin-top: 0; margin-bottom: 0; text-align:left">
            <span style="font-weight: 400">
                <font size="2">Tender Notification for One Year Rate Contract for SITC of 11 kV &amp; 33 KV Auto Recloser with Sectionalizers at TPNODL &amp; TPSODL.</font>
            </span>
        </p>
    </td>
    <td width="90" align="center" height="86" style="border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; border-bottom-style: solid; border-bottom-width: 1px">
        <font size="2">
            <a href="TPCODL435/Tender_Notice.pdf">TPCODL-435</a>
        </font>
    </td>
    <td width="88" align="center" height="86">
        <font size="2">16/02/2024</font>
    </td>
    <td align="center" height="86">
        <a href="TPCODL435/Tender_Doc.zip">
            <img border="0" src="pdf_logo.gif" width="24" height="28">
        </a>
    </td>
    <td width="124" align="center" height="86"></td>
</tr>
'''

# Parse the HTML
soup = BeautifulSoup(html, 'html.parser')

# Extract information from the <tr> element
tender_no = soup.find('font', size='2').get_text(strip=True)
date_info = soup.find('font', size='2', color='#CC0000').get_text(strip=True)
notification = soup.find('font', size='2').find_next('span').get_text(strip=True)

# Extract link from the <a> element
link = soup.find('a')['href']

print("Tender No.:", tender_no)
print("Date:", date_info)
print("Notification:", notification)
print("Link:", link)
