import re
import pandas as pd

# Read the CSV file into a DataFrame
df = pd.read_csv("D:\\TATA-POWER-Data-Fetch\\RawCentralOdissa.csv")

# Define the exclusion pattern
exclude = "sitc|construction|const|con|Supply,Installation and Commissioning|Supply,Installation and Commissioning|Laying|Erection|CIVIL|Repair"
exclude_pattern = re.compile(r'\b(?:' + '|'.join(map(re.escape, exclude.split('|'))) + r')\b', flags=re.IGNORECASE)

# Filter rows based on the exclusion pattern
filtered_df = df[~df["DESCRIPTION"].str.contains(exclude_pattern)]

# Save the filtered DataFrame to a CSV file
filtered_df.to_csv("Filtereddata.csv", index=False)
