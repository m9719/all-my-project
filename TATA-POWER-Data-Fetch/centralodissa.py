import os
import re
import textwrap
import time
import webbrowser
from datetime import datetime, timedelta
import multiprocessing as mp
import gspread
import requests
from selenium.webdriver.support import expected_conditions as EC
import pandas as pd
from selenium.webdriver.support.wait import WebDriverWait
import data_google_sheet_ops as gs
from gspread_dataframe import set_with_dataframe
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.common.exceptions import StaleElementReferenceException
from bs4 import BeautifulSoup


def open_central_odissa(url, exclude, current_date,existing_sheet_url,wks_name_in):
    updated_content = requests.get(url)
    soup = BeautifulSoup(updated_content.text, "html.parser")
    # print(soup)
    tender_link = soup.find('a', string='Tender')['href']
    print(tender_link)

    updated_content = requests.get(tender_link)
    soup = BeautifulSoup(updated_content.text, "html.parser")
    body = soup.find('body')
    tr = body.find_all('tr')
    lnk = []
    date_lst = []
    for row in tr:
        tds = row.find_all('td')
        td_texts = [' '.join(td.text.strip().replace('\xa0', '').split()) for td in tds]
        td_texts = td_texts
        lnk.append(td_texts)
    else:
        pass


    # print(lnk)
    df = pd.DataFrame(lnk)
    df = df.iloc[3:, :3]
    df = df.iloc[1:, :]
    column_names = ['DESCRIPTION', 'Tender Code', "BID SUBMISSION / TENDER FEE DUE DATE"]
    df.columns = column_names
    df['CURRENT DATE'] = current_date
    df['STATE NAME'] = "Odissa"
    df['SITE NAME'] = url.split(".")[1].removeprefix("tp")
    df['SITE LINK'] = tender_link
    print(url.split(".")[1].removeprefix("tp"))
    df = df[['CURRENT DATE', 'STATE NAME', 'SITE NAME','SITE LINK'] + column_names]
    # df.to_csv("RawCentralOdissa.csv",index=False)
    time.sleep(2)

    df = df#pd.read_csv(r"RawCentralOdissa.csv")

    # Define the exclusion pattern
    # exclude = "sitc|construction|const|con|Supply,Installation and Commissioning|Supply,Installation and Commissioning|Laying|Erection|CIVIL|Repair"
    exclude_pattern = re.compile(r'\b(?:' + '|'.join(map(re.escape, exclude)) + r')\b', flags=re.IGNORECASE)

    # Filter rows based on the exclusion pattern
    filtered_df = df[~df["DESCRIPTION"].str.contains(exclude_pattern)]

    # Save the filtered DataFrame to a CSV file
    # filtered_df.to_csv("Filtereddata.csv", index=False)
    return filtered_df


# if __name__ == "__main__":
    # url = "https://www.tpcentralodisha.com"
    # exclude = "sitc|construction|const|con|Supply, Installation and Commissioning|Supply, Installation and Commissioning|Laying|Erection|CIVIL|Repair"
    # start_time_ = time.strftime("%H-%M-%S")
    # current_date = datetime.now().strftime("%d-%m-%Y")
    # exclude = exclude.split("|")
    # open_central_odissa(url=None, exclude=None, current_date=None)
