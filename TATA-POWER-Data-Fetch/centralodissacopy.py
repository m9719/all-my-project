import os
import re
import textwrap
import time
import webbrowser
from datetime import datetime, timedelta
import multiprocessing as mp
import gspread
import requests
from selenium.webdriver.support import expected_conditions as EC
import pandas as pd
from selenium.webdriver.support.wait import WebDriverWait
import data_google_sheet_ops as gs
from gspread_dataframe import set_with_dataframe
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.common.exceptions import StaleElementReferenceException
from bs4 import BeautifulSoup


# link = requests.get(url)
# webdriver_path = r"./Driver/chromedriver.exe"  # chrome_driver_path#"D:\\C-Driver\\chromedriver.exe"
# chrome_options = webdriver.ChromeOptions()
# chrome_options.add_argument(f"webdriver.chrome.driver={webdriver_path}")

# driver = webdriver.Chrome(options=chrome_options)
# driver.get(url)
# driver.implicitly_wait(10)
#
# updated_content = driver.page_source

def open_central_odissa(url, exclude, current_date):
    updated_content = requests.get(url)
    soup = BeautifulSoup(updated_content.text, "html.parser")
    # print(soup)
    tender_link = soup.find('a', string='Tender')['href']
    print(tender_link)
    updated_content = requests.get(tender_link)
    soup = BeautifulSoup(updated_content.text, "html.parser")
    # print(soup)
    # tbody=soup.find("body")
    # # print(tbody)
    # tr=tbody.find_all("tr")
    # td=tr.find_all("td")
    body = soup.find('body')
    tr = body.find_all('tr')
    lnk = []
    date_lst = []
    for row in tr:
        tds = row.find_all('td')
        # td_texts = [td.text.strip() for td in tds]
        # link_text = td_texts[0]
        # exclude_pattern = re.compile(r'\b(?:' + '|'.join(map(re.escape, exclude)) + r')\b',
        #                              flags=re.IGNORECASE)
        # exclude_check = exclude_pattern.search(link_text)
        # if not exclude_check:
        #     # td_texts = [td.text.strip().replace('\xa0', '') for td in tds]
        td_texts = [' '.join(td.text.strip().replace('\xa0', '').split()) for td in tds]
        link_text = td_texts[0]
        exclude_pattern = re.compile(r'\b(?:' + '|'.join(map(re.escape, exclude)) + r')\b',
                                     flags=re.IGNORECASE)
        exclude_check = exclude_pattern.search(link_text)
        if not exclude_check:
        # print(td_texts)
        # print(td_texts)

            lnk.append(td_texts)
    else:
        pass


    print(lnk)
    df = pd.DataFrame(lnk)
    df = df.iloc[3:, :3]
    column_names = ['DESCRIPTION', 'PACKAGE REFERENCE NO', "BID SUBMISSION / TENDER FEE DUE DATE"]
    df.columns = column_names
    df['CURRENT DATE'] = current_date
    df['STATE NAME'] = "Odissa"
    df['SITE NAME'] = url.split(".")[1].removeprefix("tp")
    df = df[['CURRENT DATE', 'STATE NAME', 'SITE NAME'] + column_names]
    df.to_csv("CentralOdissa.csv",index=False,header=False)
    return df

if __name__ == "__main__":
    url = "https://www.tpcentralodisha.com"
    exclude = "sitc|construction|const|con|Supply, Installation and Commissioning|Supply, Installation and Commissioning|Laying|Erection|CIVIL|Repair"
    start_time_ = time.strftime("%H-%M-%S")
    current_date = datetime.now().strftime("%d-%m-%y")
    exclude = exclude.split("|")
    open_central_odissa(url, exclude, current_date)
