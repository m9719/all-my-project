import pandas as pd
import requests
from selenium import webdriver
from bs4 import BeautifulSoup
link1="https://www.tpwesternodisha.com/vendor-zone/tender"
link2="https://www.tpnodl.com/vendor-zone/tender"
link3="https://www.tpsouthernodisha.com/vendor-zone/tender"
all_links=["https://www.tpwesternodisha.com/vendor-zone/tender","https://www.tpnodl.com/vendor-zone/tender","https://www.tpsouthernodisha.com/vendor-zone/tender"]
for link in all_links:
    url = requests.get(link)
    soup = BeautifulSoup(url.content, "html.parser")
    iframe = soup.find_all("iframe")

    # Check if there is at least one iframe
    if len(iframe) >= 2:
        second_link = iframe[1]['src']
        print(second_link)
    else:
        print("No second iframe found.")


    webdriver_path = "D:\\C-Driver\\chromedriver.exe"  # Adjust the path to chromedriver.exe


    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument(f"webdriver.chrome.driver={webdriver_path}")

    driver = webdriver.Chrome(options=chrome_options)
    driver.get(second_link)
    driver.implicitly_wait(10)

    updated_content = driver.page_source


    soup=BeautifulSoup(updated_content ,"html.parser")
    body = soup.find('tbody')
    tr = body.find_all('tr')
    lnk=[]
    for row in tr:
        tds = row.find_all('td')
        td_texts = [td.text.strip() for td in tds]
        lnk.append( td_texts)
    df=pd.DataFrame(lnk)

    df=df.iloc[:, :3]
    column_names = ['DESCRIPTION', 'PACKAGE REFERENCE NO',"BID SUBMISSION / TENDER FEE DUE DATE"]
    df.columns = column_names
    site_name=link.split(".")[1]
    print(site_name)
    df.to_csv(f"{site_name}.csv",index=False)
driver.quit()