import os
import re
import time
import webbrowser
from datetime import datetime, timedelta
import multiprocessing as mp
import gspread
import requests
import json
from selenium.webdriver.support import expected_conditions as EC
import pandas as pd
from selenium.webdriver.support.wait import WebDriverWait
import data_google_sheet_ops as gs
from gspread_dataframe import set_with_dataframe
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.common.exceptions import StaleElementReferenceException
from bs4 import BeautifulSoup
from centralodissa import *


def Process_central_odissa(site_url_value, exclude, existing_sheet_url,wks_name_in,wks_name_out):
    start_time_ = time.strftime("%H-%M-%S")
    current_date = datetime.now().strftime("%d-%m-%Y")
    # exclude = exclude.split("|")
    df2 = open_central_odissa(site_url_value, exclude, current_date,existing_sheet_url,wks_name_in)
    time.sleep(1)
    gs_.add_data_(df2, sheet_name=existing_sheet_url, worksheet_name=str(wks_name_out), header=False, spacing=0,
                  table_range=None)


if __name__ == "__main__":
    with open("config.json","r")as f:
        config = json.load(f)
    start_time_ = time.strftime("%H-%M-%S")
    current_date = datetime.now().strftime("%d-%m-%Y")
    previous_date = datetime.now() - timedelta(days=1)
    previous_date = previous_date.strftime("%Y%m%d")
    mp.freeze_support()
    gs_ = gs.GoogleSheetOps(r"creds.json")
    existing_sheet_url=config["TP_Sheet_Link"]
    wks_name_in=config[ "TP_Input_Tab_Name"]
    wks_name_out=config[ "TP_Output_Tab_Name"]



    # all_links = ["https://www.tpwesternodisha.com/vendor-zone/tender", "https://www.tpnodl.com/vendor-zone/tender",
    #              "https://www.tpsouthernodisha.com/vendor-zone/tender"]

    sheet = gs_.get_worksheet_as_df(sheet_name=existing_sheet_url, worksheet_name=wks_name_in)
    # save_html_page = sheet['Save Static HTML Page'][0]
    for index, row in sheet.iterrows():
        site_url_value = row['Site Url']

        exclude = row['Exclude']
        exclude = exclude.split("|")
        central_odissa = site_url_value.split(".")[1].removeprefix("tp")
        if central_odissa == "centralodisha":
            # if site_url_value=="https://www.tpcentralodisha.com"
            # exclude = exclude.split("|")

            Process_central_odissa(site_url_value, exclude, existing_sheet_url,wks_name_in,wks_name_out)
            continue

        # for link in  site_url_value:
        url = requests.get(site_url_value)
        soup = BeautifulSoup(url.content, "html.parser")
        iframe = soup.find_all("iframe")
        if len(iframe) >= 2:
            second_link = iframe[1]['src']
            print(second_link)
        else:
            print("No second iframe found.")
        webdriver_path = r"./Driver/chromedriver.exe"  # chrome_driver_path#"D:\\C-Driver\\chromedriver.exe"
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument(f"webdriver.chrome.driver={webdriver_path}")

        driver = webdriver.Chrome(options=chrome_options)
        driver.get(second_link)
        driver.implicitly_wait(10)
        updated_content = driver.page_source
        soup = BeautifulSoup(updated_content, "html.parser")
        body = soup.find('tbody')
        tr = body.find_all('tr')
        lnk = []
        date_lst = []
        for row in tr:
            tds = row.find_all('td')
            td_texts = [td.text.strip() for td in tds]
            link_text = td_texts[0]
            exclude_pattern = re.compile(r'\b(?:' + '|'.join(map(re.escape, exclude)) + r')\b',
                                         flags=re.IGNORECASE)
            exclude_check = exclude_pattern.search(link_text)
            if not exclude_check:
                lnk.append(td_texts)
        site_name = site_url_value.split(".")[1]
        df = pd.DataFrame(lnk)
        due_dt_col = df[2]
        df = df.iloc[:, :2]
        column_names = ['DESCRIPTION', 'PACKAGE REFERENCE NO']
        df.columns = column_names
        df['CURRENT DATE'] = current_date
        df['STATE NAME'] = "Odissa"
        df['SITE NAME'] = site_name
        df['SITE LINK'] = site_url_value
        df[['BID SUBMISSION', 'TENDER FEE DUE DATE']] = due_dt_col.str.split(' / ',
                                                                             expand=True)
        # Convert the date strings to datetime objects
        df['BID SUBMISSION'] = pd.to_datetime(df['BID SUBMISSION'], format='%d-%m-%Y').dt.strftime('%d-%m-%Y')
        df['TENDER FEE DUE DATE'] = pd.to_datetime(df['TENDER FEE DUE DATE'], format='%d-%m-%Y').dt.strftime('%d-%m-%Y')

        df = df[['CURRENT DATE', 'STATE NAME', 'SITE NAME','SITE LINK'] + column_names + ["BID SUBMISSION", 'TENDER FEE DUE DATE']]
        print(site_name)
        # df.to_csv(f"{site_name}.csv", index=False)
        # sheet_name = existing_sheet_url
        gs_.add_data_(df, sheet_name=existing_sheet_url, worksheet_name=str(wks_name_out), header=False, spacing=0,
                      table_range=None)
        def time_taken(start):
            end_time_ = time.strftime("%H-%M-%S")
            total_time = (datetime.strptime(end_time_, '%H-%M-%S') - datetime.strptime(start, '%H-%M-%S')).seconds
            total_time = time.strftime("%H-%M-%S", time.gmtime(total_time))
            return end_time_, total_time
driver.quit()

end_time, total_time_taken_ = time_taken(start=start_time_)
print("Total_time_taken for TP data fetching: ", total_time_taken_)
