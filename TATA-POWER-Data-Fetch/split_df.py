import pandas as pd

# Assuming you have a DataFrame df with the "BID SUBMISSION / TENDER FEE DUE DATE" column
data = {'BID SUBMISSION / TENDER FEE DUE DATE': ['28-11-2023 / 17-11-2023',
                                                  '06-12-2023 / 27-11-2023',
                                                  '14-12-2023 / 10-10-2023']}
df = pd.DataFrame(data)

# Split the "BID SUBMISSION / TENDER FEE DUE DATE" column into two columns
df[['BID SUBMISSION', 'TENDER FEE DUE DATE']] = df['BID SUBMISSION / TENDER FEE DUE DATE'].str.split(' / ', expand=True)

# Convert the date strings to datetime objects
df['BID SUBMISSION'] = pd.to_datetime(df['BID SUBMISSION'], format='%d-%m-%Y')
df['TENDER FEE DUE DATE'] = pd.to_datetime(df['TENDER FEE DUE DATE'], format='%d-%m-%Y')

# Print the resulting DataFrame
print(df)
