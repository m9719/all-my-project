import json
import os
import re
import time
import webbrowser
from datetime import datetime, timedelta
import multiprocessing as mp
import PyPDF2
import requests
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.wait import WebDriverWait
import pandas as pd
from selenium.webdriver.support.wait import WebDriverWait
import data_google_sheet_ops as gs
from selenium import webdriver
from selenium.webdriver.common.by import By
from bs4 import BeautifulSoup
from urllib.parse import urlsplit

def main():
    global sheet_link, nbhco_inp_tab, gs_, current_date, nbhco_out_tab, sheet
    with open("config.json", "r") as file:
        config = json.load(file)
    current_date = datetime.now().strftime("%d-%m-%Y")
    previous_date = datetime.now() - timedelta(days=0)
    previous_date = previous_date.strftime("%d/%m/%Y")
    mp.freeze_support()
    gs_ = gs.GoogleSheetOps(service_account_key=r"creds.json")
    sheet_link = config["NBHCo_Sheet_Link"]
    nbhco_inp_tab = config["NBHCo_Input_Tab_Name"]
    nbhco_out_tab = config["NalCo_Output_Tab_Name"]
    sheet = gs_.get_worksheet_as_df(sheet_name=sheet_link, worksheet_name=nbhco_inp_tab)
    nalco_link = ''
    search_term = []

    for index, row in sheet.iterrows():
        if row["Site Name"] == "Nalco India":
            nalco_link = row["Link"]
            search_term = row["Search term"]

    # balco_link = "https://www.balcoindia.com/business/our-products/price-circular/"
    # src=["ec","cg","ec/cg"]
    src = search_term.split(",")
    open_url(nalco_link, src)

def open_url(nalco_link,src_text):
    webdriver_path = r"./Driver/chromedriver.exe"  # chrome_driver_path#"D:\\C-Driver\\chromedriver.exe"
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument(f"webdriver.chrome.driver={webdriver_path}")
    chrome_options.add_argument("--start-maximized")
    driver = webdriver.Chrome(options=chrome_options)

    driver.get(nalco_link)
    wait = WebDriverWait(driver, 35)
    wait.until(EC.presence_of_element_located((By.TAG_NAME, 'body')))
    content = driver.page_source
    soup = BeautifulSoup(content, 'html.parser')

    # Find the <p> tag with text "Ingots" and get the parent <a> tag
    p_tag = soup.find('p', string='Ingots')
    if p_tag:
        a_tag = p_tag.find_parent('a')
        if a_tag and 'href' in a_tag.attrs:
            link = a_tag['href']

            # Open the link in a new web browser tab
            webbrowser.open(link, new=2)
            print(link)

            response = requests.get(link)
            path = ".//Downloads"
            if not os.path.exists(path):
                os.makedirs(path)
            with open(os.path.join(path,"Nalco.pdf"), "wb") as pdf_file:
                pdf_file.write(response.content)

        with open(".//Downloads//Nalco.pdf", "rb") as pdf_file:
            # Create a PDF reader object
            pdf_reader = PyPDF2.PdfReader(pdf_file)

            # Initialize an empty list to store the extracted values
            extracted_values = []
            extracted_src = []
            entry_dict={}

            # Extract text from each page
            for page_num in range(len(pdf_reader.pages)):
                page = pdf_reader.pages[page_num]
                text = page.extract_text()

                # Use regular expression to find matches
                # matches = re.findall(r'IE07 (\d+)', text)
                for src in src_text:
                    #"IE07"
                    # matches = re.findall('{} {}'.format(src_text, re_pat), text)
                    src_str_ = fr'{src} (\d+)'
                    matches = re.findall(src_str_ , text)
                    # Append the matched values to the list
                    extracted_values.append(matches[0])
                    extracted_src.append(src)


            # Print the extracted values
            print(extracted_values)
            entry_dict["Date"] = current_date
            entry_dict["Site Name"] = "Nalco India"
            entry_dict["Link"] = link
            entry_dict["Search Key"] = extracted_src
            entry_dict["Search values"] = extracted_values
            df=pd.DataFrame(entry_dict)
            df.to_csv(os.path.join(path,"Nalco.csv"),index=False)
            gs_.add_data_(df, sheet_name=sheet_link, worksheet_name=str(nbhco_out_tab), header=False, spacing=0,
                          table_range=None)

if __name__ == "__main__":
    # with open("config.json", "r") as file:
    #     config = json.load(file)
    # current_date = datetime.now().strftime("%d-%m-%Y")
    # previous_date = datetime.now() - timedelta(days=0)
    # previous_date = previous_date.strftime("%d/%m/%Y")
    # gs_ = gs.GoogleSheetOps(service_account_key=r"creds.json")
    # sheet_link = config["NBHCo_Sheet_Link"]
    # nbhco_inp_tab = config["NBHCo_Input_Tab_Name"]
    # nbhco_out_tab = config["NBHCo_Output_Tab_Name"]
    # sheet = gs_.get_worksheet_as_df(sheet_name=sheet_link, worksheet_name=nbhco_inp_tab)
    # nalco_link = ''
    # search_term = []
    #
    # for index, row in sheet.iterrows():
    #     if row["Site Name"] == "Nalco India":
    #         nalco_link = row["Link"]
    #         search_term = row["Search term"]
    #
    # # balco_link = "https://www.balcoindia.com/business/our-products/price-circular/"
    # # src=["ec","cg","ec/cg"]
    # src = search_term.split(",")


    # nalco_link = "https://nalcoindia.com/domestic/current-price/"
    # src_text=["IE07","IE10"]
    # open_url(nalco_link,src)
    main()
