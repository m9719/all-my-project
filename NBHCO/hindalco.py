import json
import os.path
import re
import time
import webbrowser
from datetime import datetime, timedelta
import multiprocessing as mp
import PyPDF2
import pyautogui
import requests
from selenium.webdriver import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.wait import WebDriverWait
import pandas as pd
from selenium.webdriver.support.wait import WebDriverWait
import data_google_sheet_ops as gs
from selenium import webdriver
from selenium.webdriver.common.by import By
from bs4 import BeautifulSoup
from urllib.parse import urlsplit

def main():
    global sheet_link, nbhco_inp_tab, gs_, current_date, nbhco_out_tab, sheet

    with open("config.json", "r") as file:
        config = json.load(file)
    mp.freeze_support()
    current_date = datetime.now().strftime("%d-%m-%Y")
    previous_date = datetime.now() - timedelta(days=0)
    previous_date = previous_date.strftime("%d/%m/%Y")
    gs_ = gs.GoogleSheetOps(service_account_key=r"creds.json")
    sheet_link = config["NBHCo_Sheet_Link"]
    nbhco_inp_tab = config["NBHCo_Input_Tab_Name"]
    nbhco_out_tab = config["HindalCo_Output_Tab_Name"]
    sheet = gs_.get_worksheet_as_df(sheet_name=sheet_link, worksheet_name=nbhco_inp_tab)
    hindalco_link = ''
    search_term = []

    for index, row in sheet.iterrows():
        if row["Site Name"] == "Hindalco India":
            hindalco_link = row["Link"]
            search_term = row["Search term"]

    # balco_link = "https://www.balcoindia.com/business/our-products/price-circular/"
    src = search_term.split(",")

    # hindalco_link = "https://www.hindalco.com/our-businesses/aluminium-overview/primary-aluminium/primary-metal-price"
    # src=["4. EC Grade Wire Rods - Conductivity 61% min (all UTS)","5. EC Grade Wire Rods Special Cast - Conductivity 61% min (all UTS)"]
    open_url(hindalco_link, src)
def extract_price_from_pdf(pdf_file_path, target_texts, full_url,path):
    extracted_data = []  # List to store dictionaries

    with open(pdf_file_path, 'rb') as pdf_file:
        # Create a PDF reader object
        pdf_reader = PyPDF2.PdfReader(pdf_file)

        # Initialize an empty string to store the extracted text
        extracted_text = ""

        # Extract text from each page
        for page_num in range(len(pdf_reader.pages)):
            page = pdf_reader.pages[page_num]
            extracted_text += page.extract_text()

    # Use regular expression to find the price based on the target text
    for target_text in target_texts:
        matches = re.findall(fr'{re.escape(target_text)} (\d+)', extracted_text)

        # Extracted price
        if matches:
            price = matches[0]
            print(f"Extracted Price for {target_text}: {price}")

            # Create a dictionary for the extracted data
            entry_dict = {
                "Date": current_date,
                "Site Name": "Hindalco India",
                "Link": full_url,
                "Search Key": target_text,
                "Price": price
            }

            # Append the dictionary to the list
            extracted_data.append(entry_dict)
        else:
            print(f"Price for {target_text} not found in the PDF.")

    # Create a DataFrame from the list of dictionaries
    df = pd.DataFrame(extracted_data)

    # Save DataFrame to CSV
    df.to_csv(os.path.join(path,"Hindalco.csv"), index=False)

    # Add data to Google Sheets
    gs_.add_data_(df, sheet_name=sheet_link, worksheet_name=str(nbhco_out_tab), header=False, spacing=0,
                  table_range=None)
def FileDelete():

    directory_path = "./Downloads"

    # List all files in the directory
    files = os.listdir(directory_path)

    # Iterate over each file
    for file in files:
        # Check if the file starts with the specified prefix
        if file.startswith("Hindalco"):
            # Construct the full file path
            file_path = os.path.join(directory_path, file)
            # Check if the file exists and delete it
            if os.path.exists(file_path):
                os.remove(file_path)
                print(f"File '{file}' deleted successfully.")
            else:
                print(f"File '{file}' does not exist.")


def open_url(hindalco_link,src):
    FileDelete()
    webdriver_path = r"./Driver/chromedriver.exe"  # chrome_driver_path#"D:\\C-Driver\\chromedriver.exe"
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument(f"webdriver.chrome.driver={webdriver_path}")
    chrome_options.add_argument("--start-maximized")
    prefs = {"download.default_directory": "./Downloads"}
    chrome_options.add_experimental_option("prefs", prefs)
    driver = webdriver.Chrome(options=chrome_options)

    driver.get(hindalco_link)
    wait = WebDriverWait(driver, 35)
    wait.until(EC.presence_of_element_located((By.TAG_NAME, 'body')))
    content = driver.page_source
    soup = BeautifulSoup(content, 'html.parser')

    # Find the <p> tag with text "Ingots" and get the parent <a> tag
    div_tag = soup.find('div',class_="body_content")
    if div_tag:
        first_link = soup.select_one('.body_content a')['href']
        # Open the link in a new web browser tab
        url="https://www.hindalco.com"

        full_url =url+first_link
        # webbrowser.open(full_url)
        driver.get(full_url)
        time.sleep(5)
        pyautogui.hotkey("ctrl","s")
        time.sleep(5)
        path = "Downloads"
        path_=os.getcwd()
        full_path=os.path.join(path_,path)
        pyautogui.typewrite(full_path)
        pyautogui.press("enter")
        time.sleep(2)
        pyautogui.typewrite("Hindalco.pdf")
        time.sleep(1)
        pyautogui.press("enter")
        time.sleep(2)


    pdf_file_path = './/Downloads//Hindalco.pdf'
    target_text =src
    extract_price_from_pdf(pdf_file_path, target_text,full_url,path)

# main()
#
if __name__ == "__main__":
      main()