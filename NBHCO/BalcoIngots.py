import json
import os
import re
import time
import webbrowser
from datetime import datetime, timedelta
import multiprocessing as mp
from urllib import response

import PyPDF2
import requests
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.wait import WebDriverWait
import pandas as pd
from selenium.webdriver.support.wait import WebDriverWait
import data_google_sheet_ops as gs
from selenium import webdriver
from selenium.webdriver.common.by import By
from bs4 import BeautifulSoup
from urllib.parse import urlsplit

def main():
    global sheet_link, nbhco_inp_tab, gs_, current_date, nbhco_out_tab, sheet
    with open("config.json","r")as file:
        config=json.load(file)
    mp.freeze_support()
    gs_ = gs.GoogleSheetOps(service_account_key=r"creds.json")
    start_time_ = time.strftime("%H-%M-%S")
    current_date = datetime.now().strftime("%d-%m-%Y")
    previous_date = datetime.now() - timedelta(days=0)
    previous_date = previous_date.strftime("%d/%m/%Y")
    sheet_link=config["NBHCo_Sheet_Link"]
    nbhco_inp_tab=config["NBHCo_Input_Tab_Name"]
    nbhco_out_tab=config["BalCo_Output_Tab_Name"]
    sheet = gs_.get_worksheet_as_df(sheet_name=sheet_link, worksheet_name=nbhco_inp_tab)
    balco_link=''
    search_term=[]

    for index, row in sheet.iterrows():
        if row["Site Name"]=="Balco India":
            balco_link=row["Link"]
            search_term=row["Search term"]

    # balco_link = "https://www.balcoindia.com/business/our-products/price-circular/"
    # src=["ec","cg","ec/cg"]
    src=search_term.split(",")
    open_url(balco_link,src,sheet_link,nbhco_out_tab)
    # Print the extracted values
    # for match_ec in extracted_v
def open_url(balco_link,src,sheet_link,nbhco_out_tab):
    webdriver_path = r"./Driver/chromedriver.exe"  # chrome_driver_path#"D:\\C-Driver\\chromedriver.exe"
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument(f"webdriver.chrome.driver={webdriver_path}")
    chrome_options.add_argument("--start-maximized")
    driver = webdriver.Chrome(options=chrome_options)

    driver.get(balco_link)
    wait = WebDriverWait(driver, 35)
    wait.until(EC.presence_of_element_located((By.TAG_NAME, 'body')))
    content = driver.page_source
    soup = BeautifulSoup(content, 'html.parser')

    first_box = soup.find('div', class_='box-cwrapper box-1')

    if first_box:
        # Extract the publish date
        publish_date = first_box.find('span', class_='publish-date').text.strip()

        # Extract the PDF link
        pdf_link = first_box.find('a', class_='download-document')['href']

        print(f'Publish Date: {publish_date}')
        print(f'PDF Link: {pdf_link}')
    else:
        print('No matching div found.')

    response = requests.get(pdf_link)
    path = ".//Downloads"
    if not os.path.exists(path):
        os.makedirs(path)

    with open(os.path.join(path,"balcodownloaded.pdf"), "wb") as pdf_file:
        pdf_file.write(response.content)

    with open(".//Downloads//balcodownloaded.pdf", "rb") as pdf_file:
        # Create a PDF reader object
        pdf_reader = PyPDF2.PdfReader(pdf_file)

        # Initialize empty lists to store the extracted values
        extracted_values_ec = []
        extracted_values_cg = []
        extracted_values_combined = []
        # data=[]
        # Extract text from each page
        key = []
        value = []
        entry_dict={}
        for page_num in range(len(pdf_reader.pages)):
            page = pdf_reader.pages[page_num]
            text = page.extract_text()
            # data.append(text)

            # Use regular expression to find matches for EC
            matches_ec = re.findall(r'EC\s?(\d+\.\d+)%\s?(\d+)', text)
            # Use regular expression to find matches for CG with negative lookahead assertion
            matches_cg = re.findall(r'CG\s?(\d+\.\d+)%\s?(\d+)(?!\d*\.\d*% EC)', text)
            # Use regular expression to find matches for combined EC and CG
            matches_combined = re.findall(r'EC\s?(\d+\.\d+)%\s*/\s*CG\s?(\d+\.\d+)%\s*(\d+)', text)


            # Append the matched values to the respective lists
            extracted_values_ec.extend(matches_ec)
            extracted_values_cg.extend(matches_cg)
            extracted_values_combined.extend(matches_combined)
        #
        # if "ec" in src[0].lower():
        #     for match_ec in extracted_values_ec:
        #         print(f"EC {match_ec[0]}% price {match_ec[1]}")
        #         key.append(f"EC {match_ec[0]}%")
        #         value.append(f"{match_ec[1]}")
        #
        # if "cg" in src[0].lower():
        #     for match_cg in extracted_values_cg:
        #         print(f"CG {match_cg[0]}% price {match_cg[1]}")
        #         key.append(f"CG {match_cg[0]}%")
        #         value.append(f"{match_cg[1]}")

            # Print combined values for EC and CG
        if "cg" and "ec" in src[0].lower():
            for match_combined in extracted_values_combined:
                print(f"EC {match_combined[0]}% / CG {match_combined[1]}% price {match_combined[2]}")
                key.append(f"EC {match_combined[0]}% / CG {match_combined[1]}%")
                value.append(f"{match_combined[2]}")
        entry_dict["Date"] =current_date
        entry_dict["Site Name"] = "Balco India"
        entry_dict["Link"] = pdf_link
        entry_dict["Search Key"] = key
        entry_dict["Price"] = value
        df=pd.DataFrame(entry_dict)
        df.to_csv(os.path.join(path,"Balco.csv"),index=False)
        df.to_json(orient='columns')
        values = df.to_json(orient='columns')

        gs_.add_data_(df, sheet_name=sheet_link, worksheet_name=str(nbhco_out_tab), header=False, spacing=0,
                      table_range=None)

if __name__ == "__main__":
    main()
