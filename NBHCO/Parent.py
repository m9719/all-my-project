import json
import data_google_sheet_ops as gs
import hindalco
import NalcoIngots
import BalcoIngots
import multiprocessing as mp
if __name__ == "__main__":
    with open("config.json", "r") as file:
        config = json.load(file)
    mp.freeze_support()
    gs_ = gs.GoogleSheetOps(service_account_key=r"creds.json")
    sheet_link = config["NBHCo_Sheet_Link"]
    nbhco_inp_tab = config["NBHCo_Input_Tab_Name"]
    # nbhco_out_tab = config["NBHCo_Output_Tab_Name"]
    sheet = gs_.get_worksheet_as_df(sheet_name=sheet_link, worksheet_name=nbhco_inp_tab)
    hindalco_link = ''
    search_term = []

    for index, row in sheet.iterrows():
        execute_run_value = row["Execute/Run"].lower()
        site_name = row["Site Name"]

        if site_name == "Hindalco India" and execute_run_value == "yes":
            hindalco.main()
        elif site_name == "Nalco India" and execute_run_value == "yes":
            NalcoIngots.main()
        elif site_name == "Balco India" and execute_run_value == "yes":
            BalcoIngots.main()
# 
# import json
# import data_google_sheet_ops as gs
# import hindalco
# import NalcoIngots
# import BalcoIngots
# import multiprocessing as mp
# 
# def run_hindalco():
#     hindalco.main()
# 
# def run_nalco_ingots():
#     NalcoIngots.main()
# 
# def run_balco_ingots():
#     BalcoIngots.main()
# 
# if __name__ == "__main__":
#     with open("config.json", "r") as file:
#         config = json.load(file)
# 
#     mp.freeze_support()
#     gs_ = gs.GoogleSheetOps(service_account_key=r"creds.json")
#     sheet_link = config["NBHCo_Sheet_Link"]
#     nbhco_inp_tab = config["NBHCo_Input_Tab_Name"]
#     sheet = gs_.get_worksheet_as_df(sheet_name=sheet_link, worksheet_name=nbhco_inp_tab)
# 
#     processes = []
# 
#     for index, row in sheet.iterrows():
#         execute_run_value = row["Execute/Run"].lower()
#         site_name = row["Site Name"]
# 
#         if site_name == "Hindalco India" and execute_run_value == "yes":
#             processes.append(mp.Process(target=run_hindalco))
#         elif site_name == "Nalco India" and execute_run_value == "yes":
#             processes.append(mp.Process(target=run_nalco_ingots))
#         elif site_name == "Balco India" and execute_run_value == "yes":
#             processes.append(mp.Process(target=run_balco_ingots))
# 
#     for process in processes:
#         process.start()
# 
#     for process in processes:
#         process.join()
