import os
import subprocess
import pyautogui as pg
import openpyxl
import time

recently_downloaded_file = "D:\\Tender-TIGER\\Downloads\\TenderList_20240122102424.xlsx"

# Wait for the file to exist
while not os.path.exists(recently_downloaded_file):
    time.sleep(1)

# Wait for the workbook to be openable
while True:
    try:
        workbook = openpyxl.load_workbook(recently_downloaded_file)
        break
    except Exception as e:
        print(f"Error opening workbook: {e}")
        time.sleep(1)

# Wait for a specific sheet or cell to be accessible
while True:
    try:
        sheet = workbook["data"]  # Replace with your sheet name
        cell = sheet["A1"]  # Replace with your cell reference
        break
    except Exception as e:
        print(f"Error accessing sheet or cell: {e}")
        time.sleep(1)

# Now you can perform operations on the workbook, sheet, and cell
# For example, reading data from the cell
value = cell.value
print(f"Cell A1 value: {value}")
subprocess.Popen("start %windir%\explorer.exe" + " " + recently_downloaded_file, shell=True)
# df = pd.read_excel(recently_downloaded_file)
time.sleep(6)
# pg.hotkey("ctrl","s")
# time.sleep(1)
# pg.press("left")
# time.sleep(1)
# pg.press("enter")
# time.sleep(1.5)
pg.hotkey("ctrl", "a")
time.sleep(1)
pg.hotkey("ctrl", "c")
time.sleep(1)
pg.hotkey("ctrl", "v")
time.sleep(1)
pg.press("ctrl")
time.sleep(1)
pg.press("v")
time.sleep(1)
pg.press("enter")
pg.hotkey("ctrl", "s")
pg.hotkey('alt', 'f')
pg.press('c')

# Remember to close the workbook when you're done
workbook.close()
