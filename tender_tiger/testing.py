import pandas as pd

# Load the Excel file
input_file_path = "D:\\Tender-TIGER\\TenderList_20240105051545.xlsx"
df = pd.read_excel(input_file_path, engine='openpyxl')

# Filter the DataFrame based on the 'Country' column
filtered_df = df[df['Country'].isin(['bangladesh', 'nepal'])]

# Save the filtered DataFrame to a new Excel file
output_file_path = "FilteredData.xlsx"
filtered_df.to_excel(output_file_path, index=False)

print("Filtered data saved to:", output_file_path)
