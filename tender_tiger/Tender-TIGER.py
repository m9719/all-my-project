import json
import os
import subprocess
import time
from datetime import datetime, timedelta
import multiprocessing as mp
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
import data_google_sheet_ops as gs
# Set the path to the ChromeDriver executable
import pandas as pd
import pyautogui as pg
import open_windows

def minimize_window():
    # Adjust the following delay based on your system responsiveness
    # time.sleep(1)

    # Press the Windows key and the Down arrow to minimize the window
    pg.hotkey('win', 'down')


def FilterData(search_key_value, download_dir):
    recently_downloaded_file = max(
        [os.path.join(download_dir, f) for f in os.listdir(download_dir)],
        key=os.path.getctime
    )

    print("Recent downloaded file:", recently_downloaded_file)

    input_file_path = recently_downloaded_file  # "D:\\Tender-TIGER\\TenderList_20240105051545.xlsx"

    df = pd.read_excel(input_file_path)  # ,encoding='ISO-8859-1'
    # List of countries to filte
    countries_to_filter = search_key_value.split(',')

    print(countries_to_filter)
    # print(countries_to_filter)
    # countries_to_filter = [
    #     "Bangladesh", "Sri Lanka", "Nepal", "Uganda", "Ethiopia", "Dubai", "Kenya",
    #     "Rwanda", "Togo", "Zambia", "Senegal", "Ivory Coast", "Ghana", "Tanzania",
    #     "Bhutan", "Burkina Faso", "Cameroon", "DR Congo", "Gambia", "Guinea Bissau",
    #     "Guinea Conakry", "Malawi", "Mauritius", "Nigeria", "Mozambique", "Mali",
    #     "UAE", "Philippines", "Kuwait", "Burundi", "Madagascar", "Niger", "Suriname",
    #     "Switzerland", "Zimbabwe", "Zanzibar", "Jamaica", "Seychelles"
    # ]

    # Unique countries present in the "Country" column (case-insensitive)
    unique_countries_in_column = df['Country'].str.lower().unique()

    # Convert countries_to_filter to lowercase
    countries_to_filter_lower = set(country.lower() for country in countries_to_filter)

    # Find the intersection of the two lists
    countries_to_filter_lower = countries_to_filter_lower & set(unique_countries_in_column)

    # Filter the DataFrame based on the intersection of country lists
    filtered_df = df[df['Country'].str.lower().isin(countries_to_filter_lower)]

    # Check if any rows match the filtering condition
    if not filtered_df.empty:
        # Save the filtered data to a new Excel file
        path = "./Filtered File Path"
        if not os.path.exists(path):
            os.makedirs(path)
        output_file_path = os.path.join(path, 'filtered_output_file.csv')
        filtered_df.to_csv(output_file_path, index=False)
        print(f"Filtered data saved to {output_file_path}")
        return filtered_df
    else:
        print("No rows match the filtering condition. Check country names and the 'Country' column in the Excel file.")


def Tender_Tiger(site_url_value, driver, mail_id, pws, ):
    driver.get(site_url_value)
    # driver.implicitly_wait(10)
    wait = WebDriverWait(driver, 40)
    wait.until(EC.presence_of_element_located((By.TAG_NAME, 'body')))
    # Find email and password input fields
    email_input = driver.find_element(By.NAME, "Email")
    password_input = driver.find_element(By.NAME, "Password")

    # Enter your login credentials
    email_input.send_keys(mail_id)  # Replace with your actual email
    password_input.send_keys(pws)  # Replace with your actual password

    # Submit the login form
    time.sleep(1)
    password_input.send_keys(Keys.RETURN)

    time.sleep(1)
    driver.implicitly_wait(30)
    wait = WebDriverWait(driver, 40)
    wait.until(EC.presence_of_element_located((By.TAG_NAME, 'body')))
    time.sleep(5)
    select_element = WebDriverWait(driver, 50).until(
        EC.presence_of_element_located((By.ID, "profile_subno"))
    )

    # Create a Select object
    select = Select(select_element)

    # Select the option with value "287080"
    select.select_by_value("287080")
    time.sleep(2)

    download_button = WebDriverWait(driver, 40).until(
        EC.element_to_be_clickable((By.XPATH, "//a[contains(text(), 'Download All Tenders')]"))
        # You can also use the following if onclick attribute is unique:
        # EC.element_to_be_clickable((By.XPATH, "//a[@onclick='GenExcell()' and contains(text(), 'Download All Tenders')]"))
    )

    # Click the button
    download_button.click()

    time.sleep(2)

    driver.quit()

    # Set the number of times you want to minimize the window
    # num_minimizations = 5
    try:
        num_minimizations=open_windows.processes()
        # print("Initial Process: ",num_minimizations)
        num_minimizations = min(num_minimizations, 10)
        # print("After Restriction  Process: ",num_minimizations)
        # Set to 15 if it's greater than 15
        for _ in range(num_minimizations):
            minimize_window()
            # Adjust the following delay based on your needs
            time.sleep(0.5)
    except:
        pass
    format_file(download_dir)


# Wait for the login process to complete (you may need to add explicit waits here)


def format_file(download_dir):
    time.sleep(2)
    recently_downloaded_file = max(
        [os.path.join(download_dir, f) for f in os.listdir(download_dir)],
        key=os.path.getctime
    )
    print("Recent downloaded file:", recently_downloaded_file)
    subprocess.Popen('start %windir%\\explorer.exe ' + recently_downloaded_file, shell=True)

    # df = pd.read_excel(recently_downloaded_file)
    # print("Opening Excel")
    time.sleep(7)
    # print("Sleep over")
    # pg.hotkey("ctrl","s")
    # time.sleep(1)
    # pg.press("left")
    # time.sleep(1)
    # pg.press("enter")
    # time.sleep(1.5)
    # pg.press("esc")
    # time.sleep(1)
    pg.hotkey("ctrl", "a")
    time.sleep(1)
    pg.hotkey("ctrl", "c")
    time.sleep(1)
    pg.hotkey("ctrl", "v")
    time.sleep(1)
    pg.press("ctrl")
    time.sleep(1)
    pg.press("v")
    time.sleep(1)
    pg.press("enter")
    pg.hotkey("ctrl", "s")
    pg.hotkey('alt', 'f')
    pg.press('c')


if __name__ == "__main__":
    # mp.freeze_support()
    with open('config.json', 'r') as file:
        config = json.load(file)
    start_time_ = time.strftime("%H-%M-%S")
    current_date = datetime.now().strftime("%Y%m%d-%H:%M")
    previous_date = datetime.now() - timedelta(days=1)
    previous_date = previous_date.strftime("%Y%m%d")

    gs_ = gs.GoogleSheetOps(service_account_key=r"creds.json")
    service_account_key = r"creds.json"
    existing_sheet_url = config[
        "TenderTiger_Sheet_Link"]  # "https://docs.google.com/spreadsheets/d/1IzpGqVIKDqeb-QwqQL5E5uX6u32-stge46vpdCNz4lk/edit#gid=494796437"
    print("Sheet Link", existing_sheet_url)
    tiger_tender_input_tab = config["TenderTiger_Input_Tab_Name"]
    tiger_tender_output_tab = config["TenderTiger_Output_Tab_Name"]

    webdriver_path = r"./Driver/chromedriver.exe"
    # Chrome options
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument(f"webdriver.chrome.driver={webdriver_path}")
    chrome_options.add_argument("--start-maximized")
    current_dir = os.getcwd()
    download_dir = r"Downloads"  # Update with your download directory
    full_path = os.path.join(current_dir, download_dir)
    print("Full Path:", full_path)
    if not os.path.exists(full_path):
        os.makedirs(full_path)
    chrome_options.add_experimental_option("prefs", {
        "download.default_directory": full_path,
        "download.prompt_for_download": False,
        "download.directory_upgrade": True
        # "safebrowsing.enabled": True
    })
    driver = webdriver.Chrome(options=chrome_options)
    # Open the login page
    driver.delete_all_cookies()
    sheet = gs_.get_worksheet_as_df(sheet_name=existing_sheet_url, worksheet_name=tiger_tender_input_tab)
    # save_html_page = sheet['Save Static HTML Page'][0]
    for index, row in sheet.iterrows():
        site_url_value = row['Site URL']
        mail_id = row['Mail ID']
        pws = row['Password']
        search_key_value = row['Filter Countries']
        Tender_Tiger(site_url_value, driver, mail_id, pws)

        # time.sleep(5)

        time.sleep(2)
        df = FilterData(search_key_value, download_dir)
        print(df)
        gs_.add_data_(df, existing_sheet_url, tiger_tender_output_tab, header=True, spacing=0, table_range=None)
