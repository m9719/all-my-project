import pandas as pd

# Load the Excel file
input_file_path = "D:\\Tender-TIGER\\TenderList_20240105051545.xlsx"

df = pd.read_excel(input_file_path)#,encoding='ISO-8859-1'

# List of countries to filter
countries_to_filter = [
    "Bangladesh", "Sri Lanka", "Nepal", "Uganda", "Ethiopia", "Dubai", "Kenya",
    "Rwanda", "Togo", "Zambia", "Senegal", "Ivory Coast", "Ghana", "Tanzania",
    "Bhutan", "Burkina Faso", "Cameroon", "DR Congo", "Gambia", "Guinea Bissau",
    "Guinea Conakry", "Malawi", "Mauritius", "Nigeria", "Mozambique", "Mali",
    "UAE", "Philippines", "Kuwait", "Burundi", "Madagascar", "Niger", "Suriname",
    "Switzerland", "Zimbabwe", "Zanzibar", "Jamaica", "Seychelles"
]
#

# Unique countries present in the "Country" column (case-insensitive)
unique_countries_in_column = df['Country'].str.lower().unique()

# Convert countries_to_filter to lowercase
countries_to_filter_lower = set(country.lower() for country in countries_to_filter)

# Find the intersection of the two lists
countries_to_filter_lower = countries_to_filter_lower & set(unique_countries_in_column)

# Filter the DataFrame based on the intersection of country lists
filtered_df = df[df['Country'].str.lower().isin(countries_to_filter_lower)]

# Check if any rows match the filtering condition
if not filtered_df.empty:
    # Save the filtered data to a new Excel file
    output_file_path = 'filtered_output_file.csv'
    filtered_df.to_csv(output_file_path, index=False)
    print(f"Filtered data saved to {output_file_path}")
else:
    print("No rows match the filtering condition. Check country names and the 'Country' column in the Excel file.")


# import pandas as pd
# import re
#
# # Load the Excel file
# input_file_path = "C:\\Users\\PUJA AGARWAL\\Downloads\\TenderList_Web_Parent_Downloaded.xlsx"
#
# df = pd.read_excel(input_file_path)
#
# # List of countries to filter
# countries_to_filter = [
#     "Bangladesh", "Sri Lanka", "Nepal", "Uganda", "Ethiopia", "Dubai", "Kenya",
#     "Rwanda", "Togo", "Zambia", "Senegal", "Ivory Coast", "Ghana", "Tanzania",
#     "Bhutan", "Burkina Faso", "Cameroon", "DR Congo", "Gambia", "Guinea Bissau",
#     "Guinea Conakry", "Malawi", "Mauritius", "Nigeria", "Mozambique", "Mali",
#     "UAE", "Philippines", "Kuwait", "Burundi", "Madagascar", "Niger", "Suriname",
#     "Switzerland", "Zimbabwe", "Zanzibar", "Jamaica", "Seychelles"
# ]
#
# # Unique countries present in the "Country" column (case-insensitive)
# unique_countries_in_column = df['Country'].str.lower().unique()
#
# # Convert countries_to_filter to lowercase
# countries_to_filter_lower = set(country.lower() for country in countries_to_filter)
#
# # Find the intersection of the two lists
# countries_to_filter_lower = countries_to_filter_lower & set(unique_countries_in_column)
#
# # Filter the DataFrame based on the intersection of country lists
# filtered_df = df[df['Country'].str.lower().isin(countries_to_filter_lower)]
#
# # Extract display text from hyperlink formula in 'Company Name' column
# hyperlink_pattern = re.compile(r'HYPERLINK\("([^"]+)", "([^"]+)"\)')
#
# def extract_display_text(cell_value):
#     if isinstance(cell_value, str):
#         match = hyperlink_pattern.match(cell_value)
#         if match:
#             return match.group(2)
#         else:
#             return cell_value  # Return the formula if no match
#     else:
#         return cell_value  # Return non-string values unchanged
#
# # Apply the extraction function and print debug information
# filtered_df['Company Name'] = filtered_df['Company Name'].apply(extract_display_text)
#
# # Print unique values in the 'Company Name' column for further inspection
# print("Unique values in 'Company Name' column:")
# print(filtered_df['Company Name'].unique())
#
#
# # Select the columns you want to include in the filtered DataFrame
# columns_to_include = [
#     'Ref No.', 'Tender Bid Closing Date', 'Tender bid Opening Date', 'Currency',
#     'Tender Value', 'Earnest Money', 'Company Name', 'Sub Industry', 'City', 'State',
#     'Country', 'Tender Brief', 'Address', 'Tender Notice No', 'Tender Location',
#     'Keyword', 'Quantity', 'Url'
# ]
#
# # Filter the DataFrame and select the desired columns
# filtered_df = filtered_df[columns_to_include]
#
# # Check if any rows match the filtering condition
# if not filtered_df.empty:
#     # Save the filtered data to a new Excel file
#     output_file_path = 'filtered_output_file.xlsx'
#     filtered_df.to_excel(output_file_path, index=False)
#     print(f"Filtered data saved to {output_file_path}")
# else:
#     print("No rows match the filtering condition. Check country names and the 'Country' column in the Excel file.")
