import subprocess
import re

def processes():
    # PowerShell script
    ps_script = r'''
    $wd = New-Object -Com 'Word.Application'
    $visibleTasks = $wd.Tasks | Where-Object { $_.Visible }
    $numberOfProcesses = $visibleTasks.Count
    $visibleProcesses = $visibleTasks | Select-Object -ExpandProperty Name
    $wd.Quit()
    
    # Output the number of processes and process names
    Write-Output "Number of processes: $numberOfProcesses"
    Write-Output $visibleProcesses
    '''

    # Run PowerShell script
    process = subprocess.Popen(['powershell', ps_script], stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)

    # Get the output and errors
    output, errors = process.communicate()

    # Check for errors
    if process.returncode != 0:
         print(f"Error: {errors}")
    else:
        # Display the output
        # print(output)

        # Extract the number of processes from the output using regular expression
        num_processes_match = re.search(r'Number of processes: (\d+)', output)
        if num_processes_match:
            num_processes = int(num_processes_match.group(1))

            # Extract process names from the output
            process_names = set(output.splitlines()[1:])

            # print(f"Number of processes: {num_processes}")
            # print(f"Process names in set (no duplicates): {process_names}")
            return  num_processes
# processes()