import re

countries_with_inverted_commas = [
    "Bangladesh,
Sri Lanka,
Nepal,
Uganda,
Ethiopia,
Dubai,
Kenya,
Rwanda,
Togo,
Zambia,
Senegal,
Ivory Coast,
Ghana,
Tanzania
Bhutan,
Burkina Faso,
Cameroon,
DR Congo,
Gambia,
Guinea Bissau,
Guinea Conakry,
Malawi,
Mauritius,
Nigeria,
Mozambique,
Mali,
UAE,
Philippines,
Kuwait,
Burundi,
Madagascar,
Niger,
Suriname,
Switzerland,
Zimbabwe,
Zanzibar,
Jamaica,
Seychelles"
]

countries_without_inverted_commas_and_commas = [re.sub(r"[\"',]", "", country) for country in countries_with_inverted_commas]

print(countries_without_inverted_commas_and_commas)
