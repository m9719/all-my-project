countries = ["Bangladesh", "Sri Lanka", "Nepal", "Uganda", "Ethiopia", "Dubai", "Kenya",
    "Rwanda", "Togo", "Zambia", "Senegal", "Ivory Coast", "Ghana", "Tanzania",
    "Bhutan", "Burkina Faso", "Cameroon", "DR Congo", "Gambia", "Guinea Bissau",
    "Guinea Conakry", "Malawi", "Mauritius", "Nigeria", "Mozambique", "Mali",
    "UAE", "Philippines", "Kuwait", "Burundi", "Madagascar", "Niger", "Suriname",
    "Switzerland", "Zimbabwe", "Zanzibar", "Jamaica", "Seychelles"]

# Concatenate the country names into a single string separated by commas
result = ','.join(countries)

print(result)
