import re
from bs4 import BeautifulSoup

import time
from datetime import datetime, timedelta
import multiprocessing as mp
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.wait import WebDriverWait
import pandas as pd
from selenium.webdriver.support.wait import WebDriverWait
import data_google_sheet_ops as gs
from selenium import webdriver
from selenium.webdriver.common.by import By
from bs4 import BeautifulSoup
from urllib.parse import urlsplit


# Parse the HTML content
with open(r"D:\LME\LME Aluminium _ London Metal Exchange.html") as f:
    updated_content = f.read()
# lme_link ="https://www.lme.com/en/Metals/Non-ferrous/LME-Aluminium#Price+graphs"  # "https://www.lme.com/"
webdriver_path = r"D:\C-Driver\chromedriver.exe" # chrome_driver_path#"D:\\C-Driver\\chromedriver.exe"
chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument(f"webdriver.chrome.driver={webdriver_path}")
chrome_options.add_argument("--start-maximized")

driver = webdriver.Chrome(options=chrome_options)

# driver.get(lme_link)
# time.sleep(10)
wait = WebDriverWait(driver, 35)
wait.until(EC.presence_of_element_located((By.TAG_NAME, 'body')))
# updated_content = driver.page_source

try:
    # Parse the HTML content
    soup = BeautifulSoup(updated_content, 'html.parser')
    # Find all spans with class "chart__title"

    chart_title_spans = soup.find_all('span', class_='chart__title')
    # Check if any of the spans contain the expected text
    for chart_title_span in chart_title_spans:
        # print(chart_title_span)
        # print(chart_title_span.text.strip())
        if chart_title_span.text.strip()=='LME Aluminium Official Prices graph' :
            print(chart_title_span.text.strip())
            # Find the span containing the price
            price_span = soup.find('span', string=lambda x: x and 'Offer' in x)
            # Extract the price text
            price_text = price_span.text.strip() if price_span else None
            # print(price_text)
            price_regex = re.compile(r'(\d{1,3}(?:,\d{3})*(?:\.\d{1,2})?)')

            # Find matches in the string
            matches = price_regex.search(price_text)

            # Extract the price if there is a match
            price = matches.group(1) if matches else None

            # Print the extracted price
            print(price)
            # print( price_text.split(":")[1].split("(")[0])
            break  # Exit the loop once a matching span is found
    else:
        print("No span with the expected text found.")
except Exception as e:
    print("Error:", e)

def get_url(lme_link):
    webdriver_path = r"./Driver/chromedriver.exe"  # chrome_driver_path#"D:\\C-Driver\\chromedriver.exe"
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument(f"webdriver.chrome.driver={webdriver_path}")
    chrome_options.add_argument("--start-maximized")

    driver = webdriver.Chrome(options=chrome_options)

    driver.get(lme_link)
    wait = WebDriverWait(driver, 35)
    wait.until(EC.presence_of_element_located((By.TAG_NAME, 'body')))



