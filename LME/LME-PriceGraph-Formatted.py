import csv
import json
import re
import time
from datetime import datetime, timedelta
import multiprocessing as mp
import pyttsx3
import gspread
import pyautogui
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.wait import WebDriverWait
import pandas as pd
from selenium.webdriver.support.wait import WebDriverWait
import data_google_sheet_ops as gs
from selenium import webdriver
from selenium.webdriver.common.by import By
from bs4 import BeautifulSoup
from urllib.parse import urlsplit
from gspread_dataframe import set_with_dataframe, get_as_dataframe
from selenium import webdriver
from selenium.webdriver.common.by import By

import realtime
import date_cal


def add_data_to_gsheet(df, sheet_url, worksheet_name):
    """
    Add data from DataFrame to a specified worksheet in a Google Sheet.

    Parameters:
        - df: pandas DataFrame containing data to be added.
        - sheet_url: URL of the Google Sheet.
        - worksheet_name: Name of the worksheet to which data will be added.
    """
    # Authenticate with Google Sheets using service account credentials
    gc = gspread.service_account(filename='creds.json')

    # Open the Google Sheet by URL
    sh = gc.open_by_url(sheet_url)

    try:
        # Try to get the specified worksheet
        worksheet = sh.worksheet(worksheet_name)
    except gspread.WorksheetNotFound:
        # If the worksheet doesn't exist, create a new one
        worksheet = sh.add_worksheet(title=worksheet_name, rows=1000, cols=26)
    data_to_append = df.values.tolist()

    all_values = worksheet.get_all_values()

    # Determine the starting row for appending new data
    start_row = len(all_values) + 1

    # Convert DataFrame to a list of lists
    # data_to_append = [
    #     ['19-Jan-2024', 'LME-Aluminium', 'https://www.lme.com/Metals/Non-ferrous/LME-Aluminium#Price+graphs', '21305'],
    #     ['19-Jan-2024', 'LME-Copper', 'https://www.lme.com/Metals/Non-ferrous/LME-Copper#Price+graphs', '8189'],
    #     ['19-Jan-2024', 'LME-Zinc', 'https://www.lme.com/Metals/Non-ferrous/LME-Zinc#Price+graphs', '24405']]

    # Update each cell in the specified range with the new data
    for i, row in enumerate(data_to_append, start=start_row):
        for j, value in enumerate(row, start=1):
            worksheet.update_cell(i, j, value)


def scroll(driver):
    initial_height = driver.execute_script(
        "return Math.max( document.body.scrollHeight, document.body.offsetHeight, document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight );")

    # Calculate the target scroll position (e.g., 25% of the page height)
    target_scroll_position = initial_height * 0.85

    # Set the scroll step (adjust this value to control the speed of the scroll)
    scroll_step = 80

    # Scroll to the target position
    # while target_scroll_position > 0:
    for i in range(9):
        driver.execute_script(f"window.scrollTo(0, {initial_height - target_scroll_position});")
        target_scroll_position -= scroll_step
        time.sleep(1)


def voice_gen(text):
    engine = pyttsx3.init()  # object creation
    voices = engine.getProperty('voices')  # getting details of current voice
    # engine.setProperty('voice', voices[0].id)  # changing index, changes voices. 0 for male
    engine.setProperty('voice', voices[1].id)  # changing index, changes voices. 1 for female
    engine.say(text)
    engine.runAndWait()


def open_url(existing_sheet_url, metal_list):
    lme_link = "https://www.lme.com"
    webdriver_path = r"./Driver/chromedriver.exe"  # chrome_driver_path#"D:\\C-Driver\\chromedriver.exe"
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument(f"webdriver.chrome.driver={webdriver_path}")
    chrome_options.add_argument("--start-maximized")
    driver = webdriver.Chrome(options=chrome_options)
    driver.get(lme_link)
    driver.delete_all_cookies()
    wait = WebDriverWait(driver, 35)
    wait.until(EC.presence_of_element_located((By.TAG_NAME, 'body')))

    # button = WebDriverWait(driver, 30).until(
    #     EC.element_to_be_clickable((By.XPATH, "/html/body/header/div[1]/div/div/div[2]/nav/ul/li[1]/button"))
    # )
    try:
        button_xpath = '//button[@class="modal__close modal__close--float modal__close--simple"]'
        button = driver.find_element(By.XPATH, button_xpath)

        # Click on the SVG element
        button.click()
    except:
        pass
    button_locator = (By.XPATH, "/html/body/header/div[1]/div/div/div[2]/nav/ul/li[1]/button")
    button = WebDriverWait(driver, 35).until(EC.element_to_be_clickable(button_locator))
    time.sleep(1)
    button.click()
    updated_content = driver.page_source
    soup = BeautifulSoup(updated_content, "html.parser")
    body = soup.find('body')
    metal_blocks = soup.find_all('a', class_='meganav-parent-item__child-link')
    lnk = []
    link_text_lst = []
    metal_block = ""
    for metal_block in metal_blocks:
        link = metal_block['href']
        # print(link)
        if "Non" in link:
            linkdata = link
            link_text = metal_block.text
            if link.split("/")[-1] in metal_list:
                link_text_lst.append(link_text)

                # lnk.append(site_url_value)
                lnk.append(lme_link + link + "#Price+graphs")
                print(lnk)
                # print(len(lnk))
                print(len(link_text_lst))
    return lnk, link_text_lst, driver


def sub_link(lk, lk_text, driver, holiday_list):
    driver.get(lk)
    try:
        button_xpath = '//button[@class="modal__close modal__close--float modal__close--simple"]'
        button = driver.find_element(By.XPATH, button_xpath)

        # Click on the SVG element
        button.click()
    except:
        pass

    wait = WebDriverWait(driver, 45)
    wait.until(EC.presence_of_element_located((By.TAG_NAME, 'body')))
    time.sleep(1)
    metal_name = lk.split("/")[-1].split("#")[-2]
    scroll(driver)
    try:
        cookie_popup = WebDriverWait(driver, 3).until(
            EC.presence_of_element_located((By.ID, 'onetrust-button-group')))
        accept_all_cookies_button = cookie_popup.find_element(By.ID, 'onetrust-accept-btn-handler')
        # Click the accept all cookies button
        accept_all_cookies_button.click()
    except:
        pass
    # time.sleep(25)
    end_date_input = driver.find_element(By.NAME, 'end-date')
    # Get the 'max' attribute value
    max_date = end_date_input.get_attribute('max')
    print(" End Date:", max_date)
    day_before_yesterday= date_cal.date_format(holiday_list, previous_date)
    if max_date == day_before_yesterday:
        time.sleep(1)
        pyautogui.hotkey("ctrl", "f")
        time.sleep(0.5)
        pyautogui.typewrite("Official Prices")
        pyautogui.press("enter")
        time.sleep(0.50)
        # voice_gen(lk_text)
        bid_button = driver.find_element(By.XPATH, '//button[contains(text(), "Bid")]')
        bid_button.click()
        time.sleep(1)
        x, y = realtime.capture_screenshot()
        pyautogui.moveTo(x - 5, y, duration=0.60)
        time.sleep(0.80)
        pyautogui.move(0, 300, duration=2)
        time.sleep(2)
        updated_content = driver.page_source
        try:
            # Parse the HTML content
            soup = BeautifulSoup(updated_content, 'html.parser')
            # Find all spans with class "chart__title"

            chart_title_spans = soup.find_all('span', class_='chart__title')
            # Check if any of the spans contain the expected text
            for chart_title_span in chart_title_spans:
                # print(chart_title_span)
                # print(chart_title_span.text.strip())
                if chart_title_span.text.strip() == chart_title_span.text.strip():  # search_text:  # 'LME Zinc Official Prices curve':
                    print(chart_title_span.text.strip())
                    voice_gen(chart_title_span.text.strip())
                    # Find the span containing the price
                    price_span = soup.find('span', string=lambda x: x and 'Offer' in x)
                    # Extract the price text
                    price_text = price_span.text.strip() if price_span else None
                    print("Price", price_text)
                    rem_pre1 = price_text.removeprefix("Offer: ")
                    # rem_pre = rem_pre1.removeprefix("`")
                    price = rem_pre1.split("(")[0]

                    if not price:
                        print("Not Found")
                    value = price.strip()

                    voice_gen(value)
                    print("Price: ", value)

                    data = {
                        'Date': current_date,
                        'Metal': metal_name,
                        'Link': lk,
                        'Price': value
                    }
                    existing_sheet_url = config[
                        "LMEPortal_Sheet_Link"]  # "https://docs.google.com/spreadsheets/d/1IzpGqVIKDqeb-QwqQL5E5uX6u32-stge46vpdCNz4lk/edit#gid=277447040"
                    Worksheet_name = config["LMEPortal_Output_Tab_Name"]
                    # data2.append(data)
                    df = pd.DataFrame([data])
                    # Call the function to add data to Google Sheet
                    add_data_to_gsheet(df, existing_sheet_url, Worksheet_name)
                    break
                else:
                    print("No span with the expected text found.")
        except Exception as e:
            print("Error:", e)
    else:
        driver.refresh()
        sub_link(lk, lk_text, driver,holiday_list)

# existing_sheet_url = config["LMEPortal_Sheet_Link"]
#     # sheet_name = existing_sheet_url ="https://docs.google.com/spreadsheets/d/1IzpGqVIKDqeb-QwqQL5E5uX6u32-stge46vpdCNz4lk/edit#gid=277447040"
# Worksheet_name = config["LMEPortal_Output_Tab_Name"]
#     #
#     #
#
#     # Call the function to add data to Google Sheet
#
# data = pd.DataFrame(data2)
# add_data_to_gsheet(data, existing_sheet_url, Worksheet_name)


if __name__ == '__main__':
    with open('config.json', 'r') as file:
        config = json.load(file)
    start_time_ = time.strftime("%H-%M-%S")
    current_date = datetime.now().strftime("%d-%b-%Y")
    previous_date = datetime.now() - timedelta(days=1)
    previous_date = previous_date.strftime("%Y-%m-%d")
    mp.freeze_support()
    gs_ = gs.GoogleSheetOps(service_account_key=r"creds.json")
    service_account_key = r"creds.json"
    existing_sheet_url = config[
        "LMEPortal_Sheet_Link"]  # "https://docs.google.com/spreadsheets/d/1IzpGqVIKDqeb-QwqQL5E5uX6u32-stge46vpdCNz4lk/edit#gid=494796437"
    print("Sheet Link", existing_sheet_url)
    sheet = gs_.get_worksheet_as_df(sheet_name=existing_sheet_url, worksheet_name="LME Holidays")
    holiday_list = []
    for index, row in sheet.iterrows():
        dates = row['Dates']
        holiday_list.append(dates)

    metal_list = []
    lme_input_tab = config["LMEPortal_Input_Tab_Name"]
    sheet = gs_.get_worksheet_as_df(sheet_name=existing_sheet_url, worksheet_name=lme_input_tab)
    for index, row in sheet.iterrows():
        metal_name = row['Metal Name']
        metal_list.append(metal_name)
    lnk, link_text_lst, driver = open_url(existing_sheet_url, metal_list)
    for lk, lk_text in zip(lnk, link_text_lst):
        sub_link(lk, lk_text, driver, holiday_list)
