
import time
from datetime import datetime, timedelta
import multiprocessing as mp
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.wait import WebDriverWait
import pandas as pd
from selenium.webdriver.support.wait import WebDriverWait
import data_google_sheet_ops as gs
from selenium import webdriver
from selenium.webdriver.common.by import By
from bs4 import BeautifulSoup
from urllib.parse import urlsplit



def scroll(driver):
    initial_height = driver.execute_script(
        "return Math.max( document.body.scrollHeight, document.body.offsetHeight, document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight );")

    # Calculate the target scroll position (e.g., 25% of the page height)
    target_scroll_position = initial_height * 0.85

    # Set the scroll step (adjust this value to control the speed of the scroll)
    scroll_step = 80

    # Scroll to the target position
    # while target_scroll_position > 0:
    for i in range(4):
        driver.execute_script(f"window.scrollTo(0, {initial_height - target_scroll_position});")
        target_scroll_position -= scroll_step
        time.sleep(1)


def open_url(site_url_value,existing_sheet_url):
    lme_link = site_url_value#"https://www.lme.com/"
    webdriver_path = r"./Driver/chromedriver.exe"  # chrome_driver_path#"D:\\C-Driver\\chromedriver.exe"
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument(f"webdriver.chrome.driver={webdriver_path}")
    chrome_options.add_argument("--start-maximized")

    driver = webdriver.Chrome(options=chrome_options)

    driver.get(lme_link)
    wait = WebDriverWait(driver, 35)
    wait.until(EC.presence_of_element_located((By.TAG_NAME, 'body')))

    # button = WebDriverWait(driver, 30).until(
    #     EC.element_to_be_clickable((By.XPATH, "/html/body/header/div[1]/div/div/div[2]/nav/ul/li[1]/button"))
    # )
    button_locator = (By.XPATH, "/html/body/header/div[1]/div/div/div[2]/nav/ul/li[1]/button")
    button = WebDriverWait(driver, 35).until(EC.element_to_be_clickable(button_locator))
    time.sleep(1)
    button.click()
    updated_content = driver.page_source
    soup = BeautifulSoup(updated_content, "html.parser")
    body = soup.find('body')
    metal_blocks = soup.find_all('a', class_='meganav-parent-item__child-link')
    lnk = []
    link_text_lst = []
    metal_block = ""
    for metal_block in metal_blocks:
        link = metal_block['href']
        # print(link)
        if "Non" in link:
            linkdata = link
            link_text = metal_block.text
            link_text_lst.append(link_text)
            lnk.append(site_url_value + link)
    print(lnk)
    # print(len(lnk))
    # print(len(link_text_lst))

    data_lst = []
    for lk, lk_text in zip(lnk, link_text_lst):
        data_dict = {}
        driver.get(lk)
        wait = WebDriverWait(driver, 35)
        wait.until(EC.presence_of_element_located((By.TAG_NAME, 'body')))
        time.sleep(2)
        url_parts = urlsplit(lk)
        path_segments = url_parts.path.strip("/").split("/")
        last_segment = path_segments[-1]
        scroll(driver)
        time.sleep(1)
        try:
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight / 2);")
            search_ = f"{lk_text} Official Prices curve"
            print(search_)
            element = driver.find_element("xpath", f'//span[contains(text(),"{search_}")]')
            driver.execute_script("arguments[0].scrollIntoView();", element)
            updated_content = driver.page_source
            soup = BeautifulSoup(updated_content, "html.parser")
            last_offer_value = soup.find_all('td', {'data-table-column-header': 'Offer'})[-1].text.strip()
            print("Link: ", lk, "Last Offer Value:", last_offer_value, "\n")
            data_dict["Date"]=current_date
            data_dict["Metal"]=lk_text
            data_dict["Site Link"] = lk
            data_dict["Last Offer Value"] = last_offer_value
            data_lst.append(data_dict)

        except:
            try:
                updated_content = driver.page_source
                soup = BeautifulSoup(updated_content, "html.parser")
                last_offer_value = soup.find_all('td', {'data-table-column-header': 'Offer'})[-1].text.strip()
                print("Link: ", lk, "Last Offer Value:", last_offer_value)
                data_dict["Date"] = current_date
                data_dict["Metal"] = lk_text
                data_dict["Site Link"] = lk
                data_dict["Last Offer Value"] = last_offer_value
                data_lst.append(data_dict)
            except Exception as e:
                print(e)

    # print("Data List :", data_lst)
    df = pd.DataFrame(data_lst)
    # df.to_csv("LME.csv", index=False)
    sheet_name = existing_sheet_url
    Worksheet_name = "LME Details"
    gs_.add_data_(df, sheet_name=sheet_name, worksheet_name=str(Worksheet_name), header=False, spacing=0,
                  table_range=None)
    driver.close()


if __name__ == '__main__':
    start_time_ = time.strftime("%H-%M-%S")
    current_date = datetime.now().strftime("%Y%m%d-%H:%M")
    previous_date = datetime.now() - timedelta(days=1)
    previous_date = previous_date.strftime("%Y%m%d")
    mp.freeze_support()
    gs_ = gs.GoogleSheetOps(service_account_key=r"creds.json")
    service_account_key = r"creds.json"
    existing_sheet_url = "https://docs.google.com/spreadsheets/d/1IzpGqVIKDqeb-QwqQL5E5uX6u32-stge46vpdCNz4lk/edit#gid=494796437"
    print("Sheet Link", existing_sheet_url)
    sheet = gs_.get_worksheet_as_df(sheet_name=existing_sheet_url, worksheet_name="LME Input")
    for index, row in sheet.iterrows():
        site_url_value = row['Site Url']
        open_url(site_url_value,existing_sheet_url)
