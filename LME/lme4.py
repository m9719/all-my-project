from bs4 import BeautifulSoup

html_content = """
<div class="chart__main"><div class="chart__header"><h3 class="chart__header-main"><span class="chart__title">
            LME Aluminium Official Prices graph
          </span> <span class="chart__subtitle">

          </span></h3> <div class="chart__header-additional" style="display: none;">
          false
        </div></div> <ul class="chart__legend" style=""><li class="legend-item"><button class="legend-item__button"><span class="legend-item__icon-container" style="color: rgb(19, 66, 107);"><svg viewBox="0 0 24 24" aria-label="Hide chart data for " class="icon legend-item__icon legend-item__icon--hide"><use xlink:href="/dist/icons/icons.svg#show"></use></svg> <svg viewBox="0 0 24 24" aria-label="Show chart data for " class="icon legend-item__icon legend-item__icon--show"><use xlink:href="/dist/icons/icons.svg#hide"></use></svg></span>
            Bid
          </button></li><li class="legend-item"><button class="legend-item__button"><span class="legend-item__icon-container" style="color: rgb(244, 54, 76);"><svg viewBox="0 0 24 24" aria-label="Hide chart data for " class="icon legend-item__icon legend-item__icon--hide"><use xlink:href="/dist/icons/icons.svg#show"></use></svg> <svg viewBox="0 0 24 24" aria-label="Show chart data for " class="icon legend-item__icon legend-item__icon--show"><use xlink:href="/dist/icons/icons.svg#hide"></use></svg></span>
            Offer
          </button></li></ul> <div class="chart__wrapper"><!----> <!----> <canvas role="img" aria-label="" class="chart__canvas" height="463" style="display: block; box-sizing: border-box; touch-action: none; user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); height: 309px; width: 618px;" width="927"></canvas> <div class="chart-tooltip" style="top: 136.718px; left: 604.061px;"><table class="chart-tooltip__inner"><thead><tr><th>08/12/2023</th></tr></thead> <tbody class="chart-tooltip__body"><tr class="chart-tooltip__row"><td class="chart-tooltip__cell"><span class="chart-tooltip__legend" style="background-color: rgb(244, 54, 76);"></span> <span>
                    Offer: 2,111 (Expiry date 12 DEC 2023)
                  </span></td></tr></tbody></table></div></div>
"""

# Parse the HTML content
with open(r"D:\LME\LME Aluminium _ London Metal Exchange.html") as f:

    updated_content = f.read()

# soup = BeautifulSoup(updated_content, 'html.parser')
try:
    # Parse the HTML content
    soup = BeautifulSoup(updated_content, 'html.parser')

    # Find the span with class "chart__title"
    chart_title_span = soup.find('span', class_='chart__title')

    # Check if the text is "LME Aluminium Official Prices graph"
    # if chart_title_span and chart_title_span.text.strip() == 'LME Aluminium Official Prices graph':
        # Find the span containing the price
    if chart_title_span and 'LME Aluminium Official Prices' in chart_title_span.text.strip():
        # rest of the code

        price_span = soup.find('span', string=lambda x: x and 'Offer' in x)

        # Extract the price text
        price_text = price_span.text.strip() if price_span else None

        print("Offer Price:", price_text)
    else:
        print("Content of chart_title_span:", chart_title_span)
    #
    # else:
    #     print("The chart__title does not have the expected text.")
except Exception as e:
    print("Error:", e)
