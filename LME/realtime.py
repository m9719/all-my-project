import cv2
import numpy as np
import pyautogui
def capture_screenshot():
    screenshot = pyautogui.screenshot()
    image = cv2.cvtColor(np.array(screenshot), cv2.COLOR_RGB2BGR)
    image=cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    # return screenshot
    lower_red = np.array([0, 100, 100])
    upper_red = np.array([10, 255, 255])

    # Create a mask for the red color
    mask1 = cv2.inRange(image, lower_red, upper_red)

    # Additional range for red color (due to circular nature of HSV color space)
    lower_red = np.array([160, 100, 100])
    upper_red = np.array([180, 255, 255])
    mask2 = cv2.inRange(image, lower_red, upper_red)

    # Combine the masks
    mask = cv2.bitwise_or(mask1, mask2)

    # Find contours in the mask
    contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # Initialize variables for the largest bounding box
    largest_box = None
    largest_area = 0
    screenshot=None

    # Draw boxes around the detected red color
    for contour in contours:
        x, y, w, h = cv2.boundingRect(contour)

        # Check if the current bounding box is larger than the previous largest
        area = w * h
        if area > largest_area:
            largest_area = area
            largest_box = (x, y, w, h)
    if largest_box is not None:
        x, y, w, h = largest_box
        # print(f"Largest Box Coordinates: x={x+w}, y={y}")

        # Draw rectangle around the largest box
        cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)
        # Display the result
        # cv2.imwrite("output_image.jpg", image)
        # cv2.imshow("Highlighted Red Color", screenshot)
        cv2.waitKey(1)
        x = x + w
        return x,y
    else:
        print("No Chart red color detected.")
    # return largest_box

if __name__=="__main__":
    # Capture a screenshot
    x,y= capture_screenshot()
    # print(x,y)



