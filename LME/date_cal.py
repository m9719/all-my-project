from datetime import datetime, timedelta


# Example string representing a date
def date_format(input_dates, previousdate):
    day_name = datetime.now().strftime('%A')
    print(day_name)
    if day_name == "Monday":
        day_before_yesterday = datetime.now() - timedelta(days=3)
        day_before_yesterday = day_before_yesterday.strftime("%Y-%m-%d")
    elif day_name == "Sunday":
        day_before_yesterday = datetime.now() - timedelta(days=2)
        day_before_yesterday = day_before_yesterday.strftime("%Y-%m-%d")
    else:
        day_before_yesterday = datetime.now() - timedelta(days=1)
        day_before_yesterday = day_before_yesterday.strftime("%Y-%m-%d")

    for input_date in input_dates:
        date_object = datetime.strptime(input_date, "%A %d %B %Y")
        # print(f"Date as datetime object: {date_object}")
        input_date = date_object.strftime("%Y-%m-%d")
        if input_date == day_before_yesterday:
            # date_object = datetime.strptime(input_date, "%A %d %B %Y")
            # print(f"Date as datetime object: {date_object}")
            # print(f"Year: {date_object.year}")
            # print(f"Month: {date_object.month}")
            # print(f"Day: {date_object.day}")
            holiday_date = date_object.strftime("%Y-%m-%d")
            print(f"Holiday Date: {holiday_date}")
            holiday_day = date_object.strftime("%A")
            print("Holiday day :", holiday_day)
            holiday_previous_date_obj = date_object - timedelta(days=1)
            holiday_previous_date = holiday_previous_date_obj.strftime("%Y-%m-%d")
            print("Holiday Previous Date:", holiday_previous_date)
            holiday_previous_day = holiday_previous_date_obj.strftime("%A")
            print("Holiday Previous Day:", holiday_previous_day)

            if holiday_day == "Friday":
                day_before_yesterd = date_object - timedelta(days=1)
                day_before_yesterday = day_before_yesterd.strftime("%Y-%m-%d")
            if holiday_day == "Monday":
                day_before_yesterday = date_object - timedelta(days=3)
                day_before_yesterday = day_before_yesterday.strftime("%Y-%m-%d")

            else:
                day_before_yesterday = date_object - timedelta(days=1)
                day_before_yesterday = day_before_yesterday.strftime("%Y-%m-%d")
    # if not day_before_yesterday:
    #     day_name = datetime.now().strftime('%A')
    #     print(day_name)
    #     if day_name == "Monday":
    #         day_before_yesterday = datetime.now() - timedelta(days=3)
    #         day_before_yesterday = day_before_yesterday.strftime("%Y-%m-%d")
    #     elif day_name == "Sunday":
    #         day_before_yesterday = datetime.now() - timedelta(days=2)
    #         day_before_yesterday = day_before_yesterday.strftime("%Y-%m-%d")
    #     else:
    #         day_before_yesterday = datetime.now() - timedelta(days=1)
    #         day_before_yesterday = day_before_yesterday.strftime("%Y-%m-%d")


    return day_before_yesterday


# if __name__ == "__main__":
#     date_value = "Monday 1 April 2024"
#     date_format(date_value)
